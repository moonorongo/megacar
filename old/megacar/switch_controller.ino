unsigned long cebadorHastaMillis;
boolean flag = false;   

void setupSwitchController() {
  pinMode(PIN_LED_VERDE, OUTPUT);
  pinMode(PIN_LED_ROJO, OUTPUT);
  pinMode(PIN_PULSADOR, INPUT);
  pinMode(13, OUTPUT);
  digitalWrite(PIN_PULSADOR, HIGH);
  digitalWrite(PIN_LED_VERDE, HIGH);
  digitalWrite(13, LOW);
  
}
/*
void updateLed() {
  switch(cebadorStatus) {
    case 1 :  digitalWrite(PIN_LED_VERDE, HIGH);
              digitalWrite(PIN_LED_ROJO, LOW);
              break;
    case 2 :  digitalWrite(PIN_LED_VERDE, HIGH);
              digitalWrite(PIN_LED_ROJO, HIGH);
              break;
    case 3 :  digitalWrite(PIN_LED_VERDE, LOW);
              digitalWrite(PIN_LED_ROJO, HIGH);
              break;
  }
}
*/

void loopSwitchController() {

    if(digitalRead(PIN_PULSADOR)) { // si se suelta el pulsador
      if(!flag) { // si se solto por primera vez
        flag = true;
        cebadorHastaMillis = millis() + 3000; // 30 segundos
      } else { // si  sigue suelto
        if(millis() > cebadorHastaMillis) {
          // enciendo tono 
          digitalWrite(13, HIGH);
          tone( PIN_LED_ROJO, 1000, 500);
          delay(1000);
          digitalWrite(13, LOW);
          delay(500);
        }
      }
    } else { // si esta pulsado
      if(flag) { // y previamente estaba suelto
        // apago tono 
        digitalWrite(13, LOW);
        flag = false;
      }
    }
}
